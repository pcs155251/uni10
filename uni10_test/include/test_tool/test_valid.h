#ifndef __TEST_VALID_H__
#define __TEST_VALID_H__

#include "uni10.hpp"

template<typename uni10_type>
  uni10_double64 distToIdentity(uni10::Matrix<uni10_type> M)
  {
    uni10::Matrix<uni10_type> I(M.row(), M.col());
    I.identity();
    return uni10::norm(I - M);
  }

template<typename uni10_type>
  uni10_double64 distToUnitary(uni10::Matrix<uni10_type> M)
  {
    return M.row() < M.col() ? distToIdentity(uni10::dot(M, uni10::dagger(M)))
                             : distToIdentity(uni10::dot(uni10::dagger(M), M));
  }

template<typename uni10_type>
  uni10_double64 distToZero(uni10::Matrix<uni10_type> M)
  {
    return uni10::norm(M);
  }

template<typename uni10_type>
  bool isUpTri(uni10::Matrix<uni10_type> M)
  {
    for (decltype(M.row()) i = 0; i < M.row(); ++i) {
      for (decltype(M.col()) j = 0; j < i; ++j) {
        if (M[i * M.row() + j] != 0.0) return false;
      }
    }
    return true;
  }

template<typename uni10_type>
  bool isDnTri(uni10::Matrix<uni10_type> M)
  {
    for (decltype(M.row()) i = 0; i < M.row(); ++i) {
      for (decltype(M.col()) j = i + 1; j < M.col(); ++j) {
        if (M[i * M.row() + j] != 0.0) return false;
      }
    }
    return true;
  }

#endif
