#include "uni10_scalapack_mpi/uni10_elem_linalg_scalapack_mpi.h"

namespace uni10{

  void setIdentity(uni10_elem_double64* A, const uni10_bool* is_diag, const uni10_uint64* M, const uni10_uint64* N){

    if(*is_diag == true){
      
      for(uni10_uint64 i = 0; i < A->elem_num_; i++)
        A->elem_ptr_[i] = 1.;

    }
    else{
      
      uni10_linalg::setIdentity(A->elem_ptr_, *M, *N);

    }

  }

  void setIdentity(uni10_elem_complex128* A, const uni10_bool* is_diag, const uni10_uint64* M, const uni10_uint64* N){

    if(*is_diag == true){
      
      for(uni10_uint64 i = 0; i < A->elem_num_; i++)
        A->elem_ptr_[i] = 1.;

    }
    else{
      
      uni10_linalg::setIdentity(A->elem_ptr_, *M, *N);

    }

  }

}
