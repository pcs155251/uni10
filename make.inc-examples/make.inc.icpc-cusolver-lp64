# GPU_ARCH contains one or more of Fermi, Kepler, or Maxwell,
# to specify for which GPUs you want to compile MAGMA:
#     Fermi   - NVIDIA compute capability 2.x cards
#     Kepler  - NVIDIA compute capability 3.x cards
#     Maxwell - NVIDIA compute capability 5.x cards
# The default is "Fermi Kepler".
# Note that NVIDIA no longer supports 1.x cards, as of CUDA 6.5.
# See http://developer.nvidia.com/cuda-gpus
#

GPU_ARCH = Kepler
CUDADIR = /usr/local/cuda

#
# --------------------
# version name of uni10

## Name of the linear algebra solvers package.
LNPACKAGE   = cusolver

## Computational architecture.
CALARCH     = gpu

# --------------------
#
# --------------------
# programs

CC        = icc
CXX       = icpc

ARCH      = ar
ARCHFLAGS = cr
RANLIB    = ranlib


# --------------------
#
# --------------------
# flags

# UNI10 MACRO FLAGS

UNI10CXXFLAGS = -DUNI_GPU -DUNI_CUSOLVER -DUNI_MKL

# Use -fPIC to make shared (.so) and static (.a) library;
# can be commented out if making only static library.
##
## Our ATLAS installation has only static libraries, and one can't
## build a shared library against a static library, so disable FPIC.

FPIC      = -fPIC


# CPU part FLAGS :
CFLAGS       = -O3 $(FPIC) -fopenmp
LDFLAGS      =     $(FPIC)       -fopenmp
STATICFLAGS  = -Wl, -static-intel, -staticlib, --whole-archive

# GPU part FLAGS :
NVCCFLAGS    = -O3 --compiler-options '-fPIC'
NVCC_CCFLAGS =$(FPIC) -Wall -Wno-unused-function 


# USE BOOST random generators or not.
#
# CFLAGS    += -DBOOST
#

# C++11 (gcc >= 4.7) is not required, but has benefits like atomic operations

CXXFLAGS := $(UNI10CXXFLAGS) $(CFLAGS) -pthread -fPIC -fpermissive -std=c++11
CFLAGS   += -std=c99

# --------------------
#
# --------------------
# Librarys links

LDLIBRARIES := stdc++

# USE BOOST random generators or not.
#
# LDLIBRARIES    += boost_system boost_filesystem boost_thread
#
# MKL links

LDLIBRARIES += mkl_intel_lp64 mkl_intel_thread mkl_core iomp5 pthread cudart cublas cusolver gomp pthread m dl

# BLAS/LAPACK include

BLAS_INCLUDE := $(MKLROOT)/include
BLAS_LIB     := $(MKLROOT)/lib/intel64

# CUDA include
NVCC         := $(CUDADIR)/bin/nvcc
CUDA_INCLUDE := $(CUDADIR)/include
CUDA_LIB     := $(CUDADIR)/lib64

# --------------------
#
# --------------------
# customized settings

UNI10_INSTALL_PREFIX := ~/Guni10

# --------------------
#
# --------------------
# google test

GTEST := 0

# Uncomment for debugging. Does not work on OSX due to https://github.com/BVLC/caffe/issues/171
# DEBUG := 1

GPU_DEBUG := 1
# --------------------

Q ?= @
