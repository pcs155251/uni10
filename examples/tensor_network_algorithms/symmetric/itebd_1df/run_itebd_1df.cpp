#include "../../../hamiltonian/hamiltonian.h"
#include "itebd_1df_tools/itebd_1df.h"

using namespace std;
using namespace uni10;

// It is a simple example for calculating the groud state energe of Ising model by itebd in 1 dimensional system.
// The tensors utilized in this example are without considering any symmetry.
//  

UniTensor<uni10_double64> op_fermion00( const std::string& op_name ){

  Qnum q0(0);
  Qnum qf0, qf0q0, _qf0q0;
  qf0.assign(PRTF_ODD, 0, PRT_EVEN);
  qf0q0 = qf0 * q0;
  _qf0q0 = -qf0*q0;

  UniTensor<uni10_double64> op_ten;

  vector<Qnum> qf0_q0_list;
  qf0_q0_list.push_back(qf0);
  qf0_q0_list.push_back(q0);

  vector<Qnum> qf0q0_list(1, qf0q0);
  vector<Qnum> _qf0q0_list(1, _qf0q0);

  vector<Bond> bds_diag;
  bds_diag.push_back(Bond(BD_IN,  qf0_q0_list));
  bds_diag.push_back(Bond(BD_OUT, qf0_q0_list));

  vector<Bond> bds_rais;
  bds_rais.push_back(Bond(BD_IN,  qf0_q0_list));
  bds_rais.push_back(Bond(BD_OUT, qf0_q0_list));
  bds_rais.push_back(Bond(BD_OUT, qf0q0_list));
 
  vector<Bond> bds_lowr;
  bds_lowr.push_back(Bond(BD_IN,  qf0_q0_list));
  bds_lowr.push_back(Bond(BD_OUT, qf0_q0_list));
  bds_lowr.push_back(Bond(BD_OUT, _qf0q0_list));
  
  if(op_name == "N"){
    op_ten.Assign(bds_diag);
    uni10_double64 diag_elem[4] = {1., 0.,\
                                   0., 0.};
    op_ten.SetRawElem(diag_elem);
  }else if(op_name == "Id"){
    op_ten.Assign(bds_diag);
    uni10_double64 diag_elem[4] = {1., 0.,\
                                   0., 1.};
    op_ten.SetRawElem(diag_elem);
  }else if(op_name == "Cd"){
    op_ten.Assign(bds_rais);
    uni10_double64 rais_elem[4] = {0., 1.,\
                                   0., 0.};
    op_ten.SetRawElem(rais_elem);
  }else if(op_name == "C"){
    op_ten.Assign(bds_lowr);
    uni10_double64 lowr_elem[4] = {0., 0.,\
                                   1., 0.};
    op_ten.SetRawElem(lowr_elem);
  }else{
    uni10_error_msg(true, "%s", "Operator Not Supported");
  }

  op_ten.SetName(op_name);
  return op_ten;

}

UniTensor<double> tV_model(double t, double V){

  Qnum q0(0), q1(1);
  Qnum qf0, qf1, qf_1;
  qf0.assign(PRTF_ODD, 0, PRT_EVEN);
  qf1.assign(PRTF_ODD, 1, PRT_EVEN);
  qf_1 = -qf1;

  UniTensor<double> Ni = op_fermion00("N") + (-0.5) * op_fermion00("Id");
  UniTensor<double> NN = Otimes(Ni, Ni);

  UniTensor<double> CdC = Otimes(op_fermion00("Cd"), op_fermion00("C"));
  UniTensor<double> CCd = Otimes(op_fermion00("C"), op_fermion00("Cd"));

  vector<uni10_int> cmbidx;
  cmbidx.push_back(3);
  cmbidx.push_back(4);
  cmbidx.push_back(5);

  CdC.CombineBond(cmbidx);
  CCd.CombineBond(cmbidx);


  return -1.0 * (CdC + CCd) + V * NN;

}

int main(){

  Uni10Create();
  Uni10PrintEnvInfo();

  itebd_paras paras;
  paras.load_itebd_paras();

  UniTensor<uni10_double64> hamiltonian_d;
  UniTensor<uni10_complex128> hamiltonian_c;

  bool is_real = load_hamiltonian(hamiltonian_d, hamiltonian_c);

  double V = -1.0;
  double t = 0.8;

  Network theta_net("itebd_1df_net/theta.net");
  Network expv1_net("itebd_1df_net/expv1.net");
  Network expv2_net("itebd_1df_net/expv2.net");

  map<string, Network*> nets;
  nets["theta"] = &theta_net;
  nets["expv1"] = &expv1_net;
  nets["expv2"] = &expv2_net;

  hamiltonian_d = tV_model(t, V);

  if(is_real){
    iTEBD_1D<uni10_double64> itebd_run(hamiltonian_d, paras, nets);
    itebd_run.Optimize();
  }
  else{
    iTEBD_1D<uni10_complex128> itebd_run(hamiltonian_c, paras, nets);
    itebd_run.Optimize();
  }

  Uni10Destroy();

  return 0;
}
