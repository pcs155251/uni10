#include "mera_1d.h"

double Real(double c){
  return c;
}

double Real(std::complex<double> c){
  return c.real();
}

template<typename T>
MERA_1D<T>::MERA_1D(const UniTensor<T>& H, const mera_paras& paras, const map<string, Network*>& net_list)
  :rank(paras.rank), chi(paras.chi), dim(paras.dim), layers(paras.layers),
  lattices(paras.layers+1), sites(2*(int)pow(3,paras.layers-1)), epsilon(paras.epsilon),
  q_tensor(paras.q_tensor), q_layer(paras.q_layer), net_list(net_list),
  disentangler (new UniTensor<T>[layers-1]),
  coarsegrainer (new UniTensor<T>[layers-1]),
  hamiltonian (new UniTensor<T>[layers]), 
  densitymatrix (new UniTensor<T>[layers]),
  sigmaz ( new UniTensor<T>[layers])
{

  {// disentangler
    //cout << "1-1--------------------" << endl;
    vector<Bond> bdb(4, Bond(BD_IN, dim)); // bottom disentangler
    bdb[2].assign(BD_OUT, chi);
    bdb[3].assign(BD_OUT, chi);
    Matrix<T> tmp;
    UniTensor<T> utb(bdb, "[0]");
    tmp = utb.GetBlock();
    uni10_rand(tmp, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock );
    utb.PutBlock(tmp);
    disentangler[0] = utb;
    vector<Bond> bd(4, Bond(BD_IN, chi));
    bd[2].assign(BD_OUT, chi);
    bd[3].assign(BD_OUT, chi);
    for (int i=1; i<layers-1; i++){
      char buf[5]; sprintf(buf, "%d", i);
      UniTensor<T> ut(bd, "["+(string)buf+"]");
      tmp = ut.GetBlock();
      uni10_rand(tmp, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock );
      ut.PutBlock(tmp);
      disentangler[i] = ut;
    }
  }
  {// toptensor and coarsegrainer
    //cout << "2-1--------------------" << endl;
    vector<Bond> bdt(3, Bond(BD_IN, chi)); // top tensor bonds
    bdt[2].assign(BD_OUT, rank);
    Matrix<T> tmp;
    UniTensor<T> utt(bdt, "[top]");
    tmp = utt.GetBlock();
    uni10_rand(tmp, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock );
    utt.PutBlock(tmp);    // and tr(utt^dag * utt) = rank
    toptensor = utt;
    vector<Bond> bdb(4, Bond(BD_IN, chi)); // bottom coarsegrainer
    bdb[1].assign(BD_IN, dim); bdb[3].assign(BD_OUT, chi);
    UniTensor<T> utb(bdb, "[0]");
    tmp = utb.GetBlock();
    uni10_rand(tmp, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock );
    utb.PutBlock(tmp);
    coarsegrainer[0] = utb;
    vector<Bond> bd(4, Bond(BD_IN, chi));
    bd[3].assign(BD_OUT, chi);
    for (int i=1; i<layers-1; i++){
      char buf[5]; sprintf(buf, "%d", i);
      UniTensor<T> ut(bd, "["+(string)buf+"]");
      tmp = ut.GetBlock();
      uni10_rand(tmp, uni10_mt19937, uni10_uniform_real, -1, 1, uni10_clock);
      ut.PutBlock(tmp);
      coarsegrainer[i] = ut;
    }
  }
  {// utility matrix

    assert(dim == 2 && "Support spin-half systems only");

    Pz = 2.*matSz();
    Px = 2.*matSx();
    Sp = 2.*matSp();
    Sm = 2.*matSm();

    Matrix<T> mIdent(dim, dim);   
    mIdent.Identity(); Ident = mIdent;
  }
  // empty hamiltonian and densitymatrix
    UniTensor<T> ut;
    for (int i=0; i<layers; i++){
      hamiltonian[i] = ut;
      densitymatrix[i] = ut;
    }

    UniTensor<T> top = toptensor;
    UniTensor<T> topT = Dagger( top );
    int lb[] = {2, 3, 4};
    int lbo[] = {0, 1, 3, 4};
    topT.SetLabel(lb);
    UniTensor<T> Dens = Contract(topT, top);
    Dens = Permute(Dens, lbo, 2);
    densitymatrix[layers-1] = Dens;
  
  { // sigmaz
    Matrix<T> sigz = (1.0/2.0) *Otimes(Pz, Ident) + (1.0/2.0) * Otimes(Ident, Pz);
    vector<Bond> bd(4, Bond(BD_OUT, dim));
    bd[0].assign(BD_IN, dim);
    bd[1].assign(BD_IN, dim);
    UniTensor<T> ut(bd, "[0]");
    ut.PutBlock(sigz);
    sigmaz[0] = ut;
  }

  hamiltonian[0] = H;
  paras.print_info();
  cout << H;

}

template<typename T>
MERA_1D<T>::~MERA_1D(){
  delete [] disentangler;
  delete [] coarsegrainer;
  delete [] hamiltonian;
  delete [] densitymatrix;
}


template<typename T>
UniTensor<T> MERA_1D<T>::Ascend(int tau, const UniTensor<T>& Obs){

  UniTensor<T> WL = coarsegrainer[tau];
  UniTensor<T> WR = WL;
  UniTensor<T> WLT = Dagger(WL);
  UniTensor<T> WRT = Dagger(WR);
  UniTensor<T> U = disentangler[tau];
  UniTensor<T> UT = Dagger(U);

  map<string, UniTensor<T> > tens;
  tens["WL"] = WL;
  tens["WR"] = WR;
  tens["U"] = U;
  tens["Obs"] = Obs;
  tens["UT"] = UT;
  tens["WLT"] = WLT;
  tens["WRT"] = WRT;


  typename map<string, UniTensor<T> >::iterator it=tens.begin();

  std::map<string, Network*>::iterator ascL = net_list.find("ascL");
  std::map<string, Network*>::iterator ascC = net_list.find("ascC");
  std::map<string, Network*>::iterator ascR = net_list.find("ascR");

  for (; it!=tens.end(); it++){
    ascL->second->PutTensor(it->first, it->second);
    ascC->second->PutTensor(it->first, it->second);
    ascR->second->PutTensor(it->first, it->second);
  }

  UniTensor<T> ascObs, ascObs2;
  ascL->second->Launch(ascObs);
  ascC->second->Launch(ascObs2);
  ascObs += ascObs2;
  ascR->second->Launch(ascObs2);
  ascObs += ascObs2;
  ascObs *= 1.0 / 3.0;

  return ascObs;
}


template<typename T>
UniTensor<T> MERA_1D<T>::Descend(int tau, const UniTensor<T>& Dens){
  UniTensor<T> WL = coarsegrainer[tau-1];
  UniTensor<T> WR = WL;
  UniTensor<T> WLT = Dagger(WL);
  UniTensor<T> WRT = Dagger(WR);
  UniTensor<T> U = disentangler[tau-1];
  UniTensor<T> UT = Dagger(U);

  map<string, UniTensor<T>> tens;
  tens["WL"] = WL;
  tens["WR"] = WR;
  tens["U"] = U;
  tens["Dens"] = Dens;
  tens["UT"] = UT;
  tens["WLT"] = WLT;
  tens["WRT"] = WRT;

  std::map<string, Network*>::iterator desL = net_list.find("desL");
  std::map<string, Network*>::iterator desC = net_list.find("desC");
  std::map<string, Network*>::iterator desR = net_list.find("desR");

  typename map<string, UniTensor<T>>::iterator it=tens.begin();
  for (; it!=tens.end(); it++){
    desL->second->PutTensor(it->first, it->second);
    desC->second->PutTensor(it->first, it->second);
    desR->second->PutTensor(it->first, it->second);
  }

  UniTensor<T> desDens, desDens2; 
  desL->second->Launch(desDens);
  desC->second->Launch(desDens2);
  desDens += desDens2;
  desR->second->Launch(desDens2);
  desDens += desDens2;
  desDens *= 1.0 / 3.0;

  return desDens;
}


template<typename T>
void MERA_1D<T>::OptW(int tau){
  for (int q=0; q<q_tensor; q++)
  {
    UniTensor<T> Dens = densitymatrix[tau+1];
    UniTensor<T> Obs = hamiltonian[tau];
    UniTensor<T> W = coarsegrainer[tau];
    UniTensor<T> WLT = Dagger(W);
    UniTensor<T> WRT = Dagger(W);
    UniTensor<T> U = disentangler[tau];
    UniTensor<T> UT = Dagger(U); 

    map<string, UniTensor<T>> tens;
    tens["Dens"] = Dens;
    tens["W"] = W;
    tens["U"] = U;
    tens["Obs"] = Obs;
    tens["UT"] = UT;
    tens["WLT"] = WLT;
    tens["WRT"] = WRT;

    std::map<string, Network*>::iterator WLEnvL = net_list.find("WLEnvL");
    std::map<string, Network*>::iterator WLEnvC = net_list.find("WLEnvC");
    std::map<string, Network*>::iterator WLEnvR = net_list.find("WLEnvR");
    std::map<string, Network*>::iterator WREnvL = net_list.find("WREnvL");
    std::map<string, Network*>::iterator WREnvC = net_list.find("WREnvC");
    std::map<string, Network*>::iterator WREnvR = net_list.find("WREnvR");

    typename map<string, UniTensor<T>>::iterator it=tens.begin();

    for (; it!=tens.end(); it++){
      WLEnvL->second->PutTensor(it->first, it->second);
      WLEnvC->second->PutTensor(it->first, it->second);
      WLEnvR->second->PutTensor(it->first, it->second);
      WREnvL->second->PutTensor(it->first, it->second);
      WREnvC->second->PutTensor(it->first, it->second);
      WREnvR->second->PutTensor(it->first, it->second);
    }

    UniTensor<T> env, env2; 
    WLEnvL->second->Launch(env);
    WLEnvC->second->Launch(env2);
    env += env2;
    WLEnvR->second->Launch(env2);
    env += env2;
    WREnvL->second->Launch(env2);
    env += env2;
    WREnvC->second->Launch(env2);
    env += env2;
    WREnvR->second->Launch(env2);
    env += env2;

    vector< Matrix<T> > SVD = Svd(env.GetBlock());
    coarsegrainer[tau].PutBlock( -1.0 * Dagger(Dot(SVD[0], SVD[2])) );

  }
}


template<typename T>
void MERA_1D<T>::OptU(int tau){

  for (int q=0; q<q_tensor; q++)
  {
    UniTensor<T> Dens = densitymatrix[tau+1];
    UniTensor<T> Obs = hamiltonian[tau];
    UniTensor<T> WL = coarsegrainer[tau];
    UniTensor<T> WLT = Dagger(WL);
    UniTensor<T> WR = WL;
    UniTensor<T> WRT = Dagger(WR);
    UniTensor<T> UT = Dagger(disentangler[tau]);

    map<string, UniTensor<T>> tens;
    tens["Dens"] = Dens;
    tens["WL"] = WL;
    tens["WR"] = WR;
    tens["Obs"] = Obs;
    tens["UT"] = UT;
    tens["WLT"] = WLT;
    tens["WRT"] = WRT;


    std::map<string, Network*>::iterator UEnvL = net_list.find("UEnvL");
    std::map<string, Network*>::iterator UEnvC = net_list.find("UEnvC");
    std::map<string, Network*>::iterator UEnvR = net_list.find("UEnvR");
    
    typename map<string, UniTensor<T>>::iterator it=tens.begin();

    for (; it!=tens.end(); it++){

      UEnvL->second->PutTensor(it->first, it->second);
      UEnvC->second->PutTensor(it->first, it->second);
      UEnvR->second->PutTensor(it->first, it->second);

    }

    UniTensor<T> env, env2; 
    UEnvL->second->Launch(env);
    UEnvC->second->Launch(env2);
    env += env2;
    UEnvR->second->Launch(env2);
    env+=env2;

    vector< Matrix<T> > SVD = Svd( env.GetBlock() );
    disentangler[tau].PutBlock( -1.0 * Dagger( Dot(SVD[0], SVD[2])));
  }
}


template<typename T>
void MERA_1D<T>::Optimize(){

  double p_sum; p_sum = 0.0;
  double n_sum; n_sum = 1.0;
  int iter = 0;

  while (abs(n_sum - p_sum) > epsilon)
  {
    for (int l=layers-1; l>1; l--){
      // A1
      densitymatrix[l-1] = Descend(l, densitymatrix[l]);
    }
    for (int l=0; l<layers-1; l++){
      // A2
      for (int q=0; q<q_layer; q++){
        OptU(l);
        OptW(l);
      }
      // A3
      hamiltonian[l+1] = Ascend(l, hamiltonian[l]);
    }
    // A4
    vector< Matrix<T> > EIG = EigH( hamiltonian[layers-1].GetBlock() );
    vector<pair<size_t,double> > Eigvalue;
    for (size_t i=0; i<EIG[0].ElemNum(); i++){
      Eigvalue.push_back(make_pair(i, Real(EIG[0][i])));
    }
    std::sort(Eigvalue.begin(), Eigvalue.end(), comparator);
    vector<double> energy;
    for (int i=0; i<rank; i++){
      energy.push_back(Eigvalue[i].second);
      for (int j=0; j<chi*chi; j++)
        toptensor.GetBlock()[i + rank*j] = EIG[1][chi*chi*Eigvalue[i].first + j];
    }

    UniTensor<T> top = toptensor;
    UniTensor<T> topT = Dagger(top);
    int lb[] = {2, 3, 4};
    int lbo[] = {0, 1, 3, 4};
    topT.SetLabel(lb);
    UniTensor<T> Dens = Contract(topT, top);
    Permute(Dens, lbo, 2);
    densitymatrix[layers-1] = Dens;

    double ground; ground = Eigvalue[0].second;
    p_sum = n_sum;
    n_sum = std::accumulate(energy.begin(), energy.end(), 0.0);

    iter++;
    printf("iter = %4d, E/N = %14.10f, e = %9.4e\n", iter, ground, abs(n_sum-p_sum));
    //cout << "iter = " << iter << ", E/N = " << ground << ", e = " << abs(n_sum-p_sum) << endl;
  }

  for (int l=0; l<layers-1; l++)
    sigmaz[l+1] = Ascend(l, sigmaz[l]);
  Matrix<T> gs = toptensor.GetBlock(); Resize(gs, chi*chi, 1, INPLACE);
  Matrix<T> gsT = Dagger( gs );
  Matrix<T> expectsigmaz; 
  DotArgs(expectsigmaz, gsT, sigmaz[layers-1].GetBlock(), gs);
  cout << "Spontaneous magnetization = " << expectsigmaz[0]<< endl;

}

template class MERA_1D<uni10_double64>;
template class MERA_1D<uni10_complex128>;
