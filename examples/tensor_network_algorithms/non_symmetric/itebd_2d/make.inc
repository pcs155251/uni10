CXX           := g++
CXXFLAGS      := -m64 -O2 -std=c++11

UNI10CXXFLAGS := -DUNI_CPU -DUNI_LAPACK -DUNI_TCL
UNI10_VERSION := lapack_cpu

UNI10_ROOT    := $(UNI10_ROOT_CPU)
HPTT_ROOT     := $(HPTT_ROOT)
TCL_ROOT      := $(TCL_ROOT)

ROOTS := $(UNI10_ROOT) $(HPTT_ROOT) $(TCL_ROOT)
