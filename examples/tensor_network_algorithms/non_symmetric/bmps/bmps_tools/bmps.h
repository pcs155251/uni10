#ifndef __BMPS_H__
#define __BMPS_H__

#include "uni10.hpp"
#include "../../../../common/common_tools.h"

using namespace std;
using namespace uni10;

struct bmps_paras{

  bmps_paras(){
    D = 5; chi = 0;
    max_N = 1000000;
    eps = 1.0e-14; cut_off = -1.;
  }

  void Load_bmps_paras(){

    FILE* rcfp = fopen(".bmpsrc", "r");

    int max_len = 256;
    char buffer[max_len];


    char* pch;
    while(fgets(buffer, max_len, rcfp)){

      pch = strtok(buffer, ":");
      pch = strtok(NULL, ":");

      if(strcmp ("D", buffer)==0)
        D = atoi(pch);

      else if(strcmp ("chi", buffer)==0)
        chi = atoi(pch);

      else if(strcmp ("max_N", buffer)==0)
        max_N = atoi(pch);

      else if(strcmp ("eps", buffer)==0)
        eps = atof(pch);

      else if(strcmp ("cut_off", buffer)==0)
        cut_off = atof(pch);

      else if(strcmp ("measure_per_n_iter", buffer)==0)
        measure_per_n_iter = atoi(pch);

      else if(buffer[0] =='#' || pch == NULL)
        continue;

      else{
        fprintf(stdout, "%s", "Setting the parameters with wrong names.");
        exit(0);
      }

    }

    fclose(rcfp);

  }

  void print_info() const{

    fprintf(stdout, "=====================================\n");
    fprintf(stdout, "|        Parameters of iTEBD        |\n");
    fprintf(stdout, "=====================================\n");
    fprintf(stdout, "# D      : %d\n", D);
    fprintf(stdout, "# chi  : %d\n", chi);
    fprintf(stdout, "# max_N  : %lld\n", max_N);

    if(eps > 0)
      fprintf(stdout, "# eps    : 1e%.1f\n", log10(eps));
    else
      fprintf(stdout, "# eps    : %.2f\n", eps);

    if(cut_off > 0)
      fprintf(stdout, "# cut_off: 1e%.1f\n", log10(cut_off));
    else
      fprintf(stdout, "# cut_off: %.2f\n", cut_off);

    fprintf(stdout, "# measure_per_n_iter: %d\n", measure_per_n_iter);
    fprintf(stdout, "=====================================\n");
  }

  int D, chi, measure_per_n_iter;     // D: Virtual bonds dimension.
                       // d: Physical bonds dimension.
                       // chi: boundary bonds dimesnion.
  long long int max_N; // max_N: Upper bondary of update interations.
  double eps, cut_off;

};

template<typename T>
class twoSiteiMPS{
  public:
    twoSiteiMPS( ) { } //default contructor
    twoSiteiMPS( const int dimMPS, const vector<UniTensor<T>>& evolOperatorIn );
    bool fixedPoint( const int maxIter, const double errTor, double &trunErr );
    UniTensor<T> contractTheta( const int iSite);
    UniTensor<T> contractThetaBalance( const int iSite);//bond start at top left counterclockwisely, two in two out
    T measureTwoSite( UniTensor<T> &twoSiteOp, const int iSite );
    const vector<UniTensor<T>>& getEvolOperator( ) const { return evolOperator; }
    const UniTensor<T>& getGammaLBalance( ) const { return gammaLBalance; };
    const UniTensor<T>& getGammaRBalance( ) const { return gammaRBalance; };
  private:
    int dimPhy;  
    int dimMPS; 
    vector<UniTensor<T>> evolOperator;
    vector<UniTensor<T>> gammas;
    vector<Matrix<T>> lambdas;
    UniTensor<T> gammaLBalance;
    UniTensor<T> gammaRBalance;
    double timeEvoAstep( UniTensor<T> &evoOperator, const int iSite);
    UniTensor<T> contractCten( const int iSite);
};

template<typename T>
class Bmps{

  public:
    Bmps(const UniTensor<T>& _H, const bmps_paras& paras);

    ~Bmps();

    void setHamiltonian(const UniTensor<T>& _H);

    void initialize(const string& loadDir);

    void Optimize();

    double measureExpec( const UniTensor<T> &op ); 

    UniTensor<T> get_gate(const UniTensor<T>& H);

  private:
    uni10_int dim;      // Physical bond dimension.
    uni10_int D;        // Virtual bond dimension.
    uni10_double64 eps; // 
    uni10_uint64 max_N; // Maximun iteration number.
    uni10_int cut_off;  // The cut off for dynamic truncation.
    uni10_int chi;    // Virtual bond dimension for boundary MPS.
    uni10_int measure_per_n_iter;

    vector<UniTensor<T> > gammas;
    vector<Matrix<T> > lambdas;
	
    UniTensor<T> H;       // Hamiltonian filled of real components.

    //for bMPS
    twoSiteiMPS<T> mpsUp;
    twoSiteiMPS<T> mpsDn;
    UniTensor<complex<double>> lefTen;
    UniTensor<complex<double>> rigTen;
    void LoadTensors(const string& loadDir);
    void bondsqrtcatAll();
    void measureNorm();
    double currentNorm;
    Network transferMatrix_net;
    Network bmpsNorm_net;
    Network bmpsExpec_net;
};

#endif
