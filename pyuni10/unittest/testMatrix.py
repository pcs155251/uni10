from pyUni10 import MatrixR
from pyUni10 import MatrixC

## Size of matrix

class size(object):

    def __init__(self, r, c):
        self.row = r
        self.col = c

sizes = [size(8, 3), size(5, 5), size(3, 8)]

## Generate test files

test_elem_not_diag = []
test_elem_diag = []
suffix = ("A", "B", "C")
i = 0
for size in sizes:
    test_elem_not_diag.append([float(x) for x in range(size.row*size.col)])
    test_elem_diag.append([float(x) for x in range(min(size.row, size.col))])

for i in range(3):
    size = sizes[i]
    src = test_elem_not_diag[i]
    M_not_diag = MatrixR(size.row, size.col)
    M_not_diag.SetElem(src)
    M_not_diag.Save("testMatrixd_notDiag{}".format(suffix[i]))

    src = test_elem_diag[i]
    M_diag = MatrixR(size.row, size.col, True)
    M_diag.SetElem(src)
    M_diag.Save("testMatrixd_Diag{}".format(suffix[i]))

