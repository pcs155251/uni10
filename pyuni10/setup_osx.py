import platform
from distutils.core import setup, Extension
from distutils import sysconfig

uni10_api = ["../src/uni10_api/Qnum.cpp",
			 "../src/uni10_api/Bond.cpp",
			 "../src/uni10_api/Block.cpp",
			 "../src/uni10_api/Matrix.cpp",
			 "../src/uni10_api/Matrix_Auxiliary.cpp",
			 "../src/uni10_api/UniTensor.cpp",
			 "../src/uni10_api/UniTensor_Auxiliary.cpp",
			 "../src/uni10_api/tensor_tools/tensor_tools.cpp",
			 "../src/uni10_api/Network.cpp",
			 "../src/uni10_api/network_tools/layer.cpp",
			 "../src/uni10_api/network_tools/netorder.cpp",
			 "../src/uni10_api/network_tools/network_tools.cpp",
			 "../src/uni10_api/network_tools/node.cpp",
			 "../src/uni10_api/network_tools/pseudotensor.cpp"]

lapack_dir = ["../src/uni10_lapack_cpu/uni10_elem_lapack_cpu.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_add.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_add_v.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_conj.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_dagger.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_det.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_dot.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_eig.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_eigh.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_exp_v.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_identity.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_inverse.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_ldq.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_lq.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_mul.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_mul_v.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_norm.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_normalrandomize.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_qdr.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_qdrcpivot.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_ql.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_qr.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_r2c.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_rq.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_scal_v.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_sdd.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_setdiag.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_sub.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_sub_v.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_sum_v.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_svd.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_sytrimateigdcp.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_trace.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_transpose.cpp",
			  "../src/uni10_lapack_cpu/linalg/uni10_elem_uniformrandomize.cpp",
			  "../src/uni10_lapack_cpu/hirnk_linalg/hptt_tcl/uni10_elem_tensorcontract.cpp",
			  "../src/uni10_lapack_cpu/hirnk_linalg/hptt_tcl/uni10_elem_tensortranspose.cpp",
			  "../src/uni10_lapack_cpu/hirnk_linalg/ttgt/uni10_elem_tensorcontract.cpp",
			  "../src/uni10_lapack_cpu/hirnk_linalg/ttgt/uni10_elem_tensortranspose.cpp",
			  "../src/uni10_lapack_cpu/tools_lapack_cpu/uni10_tools_lapack_cpu.cpp",
			  "../src/uni10_lapack_cpu/tools_lapack_cpu/uni10_linalg_lapack_cpu_d.cpp",
			  "../src/uni10_lapack_cpu/tools_lapack_cpu/uni10_linalg_lapack_cpu_dz.cpp",
			  "../src/uni10_lapack_cpu/tools_lapack_cpu/uni10_linalg_lapack_cpu_z.cpp"]
			  
env_info = ["../src/uni10_env_info/uni10_lapack_cpu/uni10_env_info_lapack_cpu.cpp"]

cpp_args = ["-std=c++11", "-stdlib=libc++", "-DUNI_CPU", "-DUNI_LAPACK", "-D_hypot=hypot",
            "-mmacosx-version-min=10.9"]

wrap = ["pyUni10.cpp"]

src = uni10_api + lapack_dir + env_info + wrap

if platform.architecture()[0] == "32bit":
	lib_dir = "dep/x86"
else:
	lib_dir = "dep/x64"

ext_modules = [
	Extension("pyUni10", sources=src, 
			  include_dirs=["pybind11/include", "../include"],
			  libraries=["blas", "lapack"],
			  language="c++", extra_compile_args=cpp_args)]

setup(
	name="pyUni10", version="2.0.0",ext_modules=ext_modules)
